﻿using System;

namespace ChemQuiz {
  enum VraagSoort {
    ZoekSymbool,
    ZoekAtoomNummer,
    ZoekNaam
  }

  class Program {
    static void Main(string[] args) {
      string[] elNamen = {
        "Hydrogen", "Helium", "Lithium", "Beryllium", "Boron", "Carbon",
        "Nitrogen", "Oxygen", "Fluorine", "Neon", "Sodium", "Magnesium",
        "Aluminum", "Silicon", "Phosphorus", "Sulfur", "Chlorine", "Argon",
        "Potassium", "Calcium", "Scandium", "Titanium", "Vanadium", "Chromium",
        "Manganese", "Iron", "Cobalt", "Nickel", "Copper", "Zinc", "Gallium",
        "Germanium", "Arsenic", "Selenium", "Bromine", "Krypton", "Rubidium",
        "Strontium", "Yttrium", "Zirconium", "Niobium", "Molybdenum",
        "Technetium", "Ruthenium", "Rhodium", "Palladium", "Silver", "Cadmium",
        "Indium", "Tin", "Antimony", "Tellurium", "Iodine", "Xenon", "Cesium",
        "Barium", "Lanthanum", "Cerium", "Praseodymium", "Neodymium",
        "Promethium", "Samarium", "Europium", "Gadolinium", "Terbium",
        "Dysprosium", "Holmium", "Erbium", "Thulium", "Ytterbium", "Lutetium",
        "Hafnium", "Tantalum", "Tungsten", "Rhenium", "Osmium", "Iridium",
        "Platinum", "Gold", "Mercury", "Thallium", "Lead", "Bismuth",
        "Polonium", "Astatine", "Radon", "Francium", "Radium", "Actinium",
        "Thorium", "Protactinium", "Uranium", "Neptunium", "Plutonium",
        "Americium", "Curium", "Berkelium", "Californium", "Einsteinium",
        "Fermium", "Mendelevium", "Nobelium", "Lawrencium", "Rutherfordium",
        "Dubnium", "Seaborgium", "Bohrium", "Hassium", "Meitnerium",
        "Darmstadtium", "Roentgenium", "Copernicium", "Nihonium", "Flerovium",
        "Moscovium", "Livermorium", "Tennessine", "Oganesson"
      };
      string[] elSymbool = {
        "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al",
        "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn",
        "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb",
        "Sr", "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In",
        "Sn", "Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm",
        "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu", "Hf", "Ta",
        "W", "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At",
        "Rn", "Fr", "Ra", "Ac", "Th", "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk",
        "Cf", "Es", "Fm", "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt",
        "Ds", "Rg", "Cn", "Nh", "Fl", "Mc", "Lv", "Ts", "Og"
      };
      int score = 0;

      //setup ronde 1
      Console.Write("Aantal vragen in ronde 1: ");
      int numVragen = Convert.ToInt32(Console.ReadLine());
      Random r = new Random();

      //ronde 1 - stel vraag
      for (int i = 0; i < numVragen; i++) {
        score += StelVraag(elNamen, elSymbool, (VraagSoort)r.Next(0, 3));
      }
      Console.WriteLine("=> Score ronde 1: " + score);

      //ronde 2 - sudden death
      score += SuddenDeath(elNamen, elSymbool);
      Console.WriteLine("==> You finished! Your final score: " + score);
    }

    private static int SuddenDeath(string[] namen, string[] symbolen) {
      int totalScore = 0;
      bool shouldRun = true;
      do {
        int score = StelVraag(namen, symbolen, VraagSoort.ZoekSymbool);
        totalScore += score;
        
        if (score == 0) 
          shouldRun = false;
      } while (shouldRun);
      Console.WriteLine("=> Score ronde 2: " + totalScore);
      return totalScore;
    }

    private static int StelVraag(string[] namen, string[] symbolen,
      VraagSoort vraagSoort) {
      int index = new Random().Next(1, namen.Length);
      string elName = namen[index];
      string elSymbol = symbolen[index];
      int elNum = index + 1;

      string answer;
      switch (vraagSoort) {
        case VraagSoort.ZoekSymbool:
          Console.Write("Wat is het symbool van {0} (num: {1})? ", elName,
            elNum);
          answer = Console.ReadLine();
          if (answer.Equals(elSymbol))
            return 1;
          break;

        case VraagSoort.ZoekAtoomNummer:
          Console.Write("Wat is het atoomnummer van {0} (symbol: {1})? ",
            elName,
            elSymbol);
          answer = Console.ReadLine();
          if (answer.Equals(elNum))
            return 1;
          break;

        case VraagSoort.ZoekNaam:
          Console.Write("Wat is de naam van {0} (num: {1})? ", elSymbol, elNum);
          answer = Console.ReadLine();
          if (answer.Equals(elName))
            return 1;
          break;

        default:
          Console.WriteLine("ERROR: VraagSoort {0} is niet geldig: ",
            vraagSoort);
          break;
      }
      Console.WriteLine("> Fout! {{num:{2}, name:{0}, sym:{1}}}", elName, elSymbol, elNum);
      return 0;
    }
  }
}